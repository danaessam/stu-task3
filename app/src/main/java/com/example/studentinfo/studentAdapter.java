package com.example.studentinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public  class studentAdapter extends RecyclerView.Adapter<studentAdapter.ViewHolder> {

    TextView name, id, level, average;


    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.stu_name);
            id = itemView.findViewById(R.id.stu_id);
            level = itemView.findViewById(R.id.stu_level);
            average = itemView.findViewById(R.id.stu_avg);
        }

    }

    private Context context;
    private List<student> students;

    public studentAdapter(Context c, List<student> studentList) {
        this.context = context;
        students = studentList;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.activity_student_info, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        student stu = students.get(position);


    }

    @Override
    public int getItemCount() {
        return students.size();
    }


    public void setData(student s) {
        name.setText(s.getName());
        id.setText(s.getId());
        level.setText(s.getLevel());
        average.setText(s.getAverage());


    }
}