package com.example.studentinfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private studentAdapter studentAdapter;
    private ArrayList<student> students;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            recyclerView = findViewById(R.id.stu_vr);
            students = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                students.add(new student("Ahmed" + i, "2018977" + i, "Second" + i, 87));
                students.add(new student("Rania" + i, "2017734" + i, "Third" + i, 70));
                students.add(new student("Soad" + i, "2016234" + i, "Forth" + i, 80));
                students.add(new student("Hani" + i, "2015985" + i, "Fifth" + i, 90));


            }


            studentAdapter = new studentAdapter(this, students);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(studentAdapter);


        }
 public void click(View view){
        EditText name=findViewById(R.id.stu_vr);
        EditText id=findViewById(R.id.stu_vr);
        EditText level=findViewById(R.id.stu_vr);
        EditText average=findViewById(R.id.stu_vr);
     Intent intent=new Intent(this,studentInfo.class );
     Bundle b=new Bundle();
     b.putString("name",name.getText().toString());
     b.putString("id",id.getText().toString());
     b.putString("level",level.getText().toString());
     b.putString("average",average.getText().toString());
     intent.putExtras(b);
     startActivity(intent);



 }


    }




