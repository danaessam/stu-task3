package com.example.studentinfo;

public class student {
    String name;
    String id;
    String level;
    int average;

    public student(String name, String id, String level,int average) {
        this.name = name;
        this.id = id;
        this.level = level;
        this.average = average;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }
}

